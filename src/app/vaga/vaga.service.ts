import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vaga } from './vaga.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VagaService {
  private apiUrl = `${environment.apiUrl}/api/vaga`; 

  constructor(private http: HttpClient) {}

  getAll(): Observable<Vaga[]> {
    return this.http.get<Vaga[]>(this.apiUrl);
  }
  addVaga(vaga: Vaga){
    return this.http.post(this.apiUrl, vaga);
  }
  updateVaga(vaga: Vaga){
    return this.http.patch(this.apiUrl, vaga);
  }
  removeVaga(id:string){
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
}