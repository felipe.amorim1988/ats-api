// vaga.component.ts
import { Component, OnInit } from '@angular/core';
import { VagaService } from './vaga.service';
import { CandidaturaService } from '../candidatura/candidatura.service';
import { PoTableAction } from '@po-ui/ng-components';
import { Vaga } from './vaga.model';

@Component({
  selector: 'app-vaga',
  templateUrl: './vaga.component.html',
  styleUrls: ['./vaga.component.css'],
})

export class VagaComponent implements OnInit {
  vagas: any[] = [];
  vaga: Vaga = {
    id: undefined,
    titulo: '',
    descricao: ''
  };
  candidaturas: any[] = [];
  check: boolean = false;
  actions: Array<PoTableAction> = [
    { action: this.remove.bind(this), icon: 'po-icon po-icon-delete', label: 'Remover' },
    { action: this.details.bind(this), icon: 'po-icon-export', label: 'Editar' },
    { action: this.checkVacancy.bind(this), icon: 'po-icon-export', label: 'Candidaturas' },
  ];
  columns = [
    { property: 'id' },
    { property: 'titulo' },
    { property: 'descricao' },
  ];
  columnsC = [
    { property: 'nome' },
    { property: 'cpf' },
  ];

  constructor(private vagaService: VagaService, private candidaturaService: CandidaturaService) { }

  ngOnInit(): void {
    this.carregarVagas();
  }
  addVaga(){
    if(!this.vaga.id)
    {
      this.vagaService.addVaga(this.vaga).subscribe(data=>this.carregarVagas())
    }else{
      this.vagaService.updateVaga(this.vaga).subscribe(data=>this.carregarVagas())
    }

  }
  toogleCheck() {
    this.check = !this.check
  }
  checkVacancy(row: any) {
    console.log(row)
    this.candidaturaService.getByVaga(row.id).subscribe(data => this.candidaturas = data)
    this.toogleCheck()
  }
  remove(row: any) {
    console.log(row)
    this.vagaService.removeVaga(row.id).subscribe(data=>{
      this.carregarVagas();
    });
  }
  details(row: any) {
    console.log(row)
    this.vaga = {
      id: row.id,
      titulo: row.titulo,
      descricao: row.descricao
    };
  }
  carregarVagas() {
    this.vagaService.getAll().subscribe(data => {
      this.vagas = data;
      this.vaga = {
        id: undefined,
        titulo: '',
        descricao: ''
      };
    });
  }
}
