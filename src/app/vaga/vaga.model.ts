export interface Vaga {
    id?: string;
    titulo: string;
    descricao: string;
  }