import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PoTableAction } from '@po-ui/ng-components';
import { VagaService } from '../vaga/vaga.service';
import { CandidaturaService } from './candidatura.service';
@Component({
  selector: 'app-candidatura',
  templateUrl: './candidatura.component.html',
  styleUrls: ['./candidatura.component.css']
})
export class CandidaturaComponent implements OnInit {
  pessoaId: string = '';
  vagas: any[] = []
  actions: Array<PoTableAction> = [
    { action: this.addCandidatura.bind(this), icon: 'po-icon po-icon-ok', label: 'Confirmar' }
  ];
  columns = [
    { property: 'titulo' },
    { property: 'descricao' },
  ];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private vagaService: VagaService,
    private candidaturaService: CandidaturaService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.pessoaId = params['id'];
    });
    this.carregarVagas();
  }
  carregarVagas() {
    this.vagaService.getAll().subscribe(data => this.vagas = data)
  }
  addCandidatura(row: any) {
this.candidaturaService.addCandidatura(this.pessoaId, row.id)
.subscribe(data =>this.router.navigate(['/vagas']))
  }

  obtercandidaturas(): void {

  }
}