import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Candidato } from '../candidato/candidato.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CandidaturaService {
  private apiUrl = `${environment.apiUrl}/api/candidatura`; 

  constructor(private http: HttpClient) {}

  getByVaga(VagaId:string): Observable<Candidato[]> {
    return this.http.get<Candidato[]>(`${this.apiUrl}/${VagaId}`);
  }
  addCandidatura(CandidatoId: string, VagaId: string){
    return this.http.post(`${this.apiUrl}/${CandidatoId}/${VagaId}`,null);
  }

}