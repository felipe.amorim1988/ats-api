export interface Candidato {
    id: string;
    nome: string;
    cpf: string;
    cv: any;
  }