// candidato.component.ts
import { Component, ElementRef, OnInit,ViewChild } from '@angular/core';
import { CandidatoService } from './candidato.service';
import { PoTableAction } from '@po-ui/ng-components';
import { Candidato } from './candidato.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-candidato',
  templateUrl: './candidato.component.html',
  styleUrls: ['./candidato.component.css'],
})

export class CandidatoComponent implements OnInit {
  candidatos: any[] = [];
  novoCandidato: Candidato = {
    id: '',
    nome: '',
    cpf: '',
    cv: undefined
  };
  file: any = {};
  @ViewChild('myInput')
  myInputVariable!: ElementRef;
  actions: Array<PoTableAction> = [
    { action: this.remove.bind(this), icon: 'po-icon po-icon-delete', label: 'Remover' },
    { action: this.details.bind(this), icon: 'po-icon-export', label: 'Editar' },
    { action: this.searchJobs.bind(this), icon: 'po-icon-warehouse', label: 'Vagas' },
  ];
  columns = [
    { property: 'id'},
    { property: 'nome'},
    { property: 'cpf'},
  ];

  constructor(private candidatoService: CandidatoService,private router: Router) { }

  ngOnInit(): void {
    this.carregarcandidatos();
  }
  searchJobs(row:any){
    console.log(row)
    this.router.navigate(['/candidatura', row.id]);

  }
remove(row:any){
  console.log(row)
  this.removeCandidato(row.id)
}
details(row:any){
  console.log(row)
  this.myInputVariable.nativeElement.value = "";
  this.novoCandidato = {
    id: row.id,
    nome: row.nome,
    cpf: row.cpf,
    cv: null
  };
}
  carregarcandidatos() {
    this.candidatoService.getAll().subscribe(data => {
      this.candidatos = data;
    });
  }
  onFilechange(event: any) {
    this.novoCandidato.cv = event.target.files[0]
  }
  removeCandidato(id:string){
    this.candidatoService.removeCandidato(id).subscribe(data=>{
      this.reloadScreen()
    });
  }
  reloadScreen(){
    this.carregarcandidatos()  
    this.clearCandidato() 
  }
  addCandidato(){
    if(this.novoCandidato.id == ''){
      this.candidatoService.addCandidato(this.novoCandidato).subscribe(data =>    this.reloadScreen()
      );
    }else{
      this.candidatoService.updateCandidato(this.novoCandidato).subscribe(data =>    this.reloadScreen()
      );
    }
  }
  clearCandidato(){
    this.myInputVariable.nativeElement.value = "";
    this.novoCandidato = {
      id: '',
      nome: '',
      cpf: '',
      cv: undefined
    };
  }
}
