import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Candidato } from './candidato.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CandidatoService {
  private apiUrl = `${environment.apiUrl}/api/candidato`; 

  constructor(private http: HttpClient) {}

  getAll(): Observable<Candidato[]> {
    return this.http.get<Candidato[]>(this.apiUrl);
  }
  addCandidato(candidato: Candidato){
    const formData = new FormData();
    formData.append('nome', candidato.nome);
    formData.append('cpf', candidato.cpf);
    formData.append('cv', candidato.cv);

    return this.http.post(this.apiUrl, formData);
  }
  updateCandidato(candidato: Candidato){
    const formData = new FormData();
    formData.append('id', candidato.id);
    formData.append('nome', candidato.nome);
    formData.append('cpf', candidato.cpf);
    formData.append('cv', candidato.cv);
    return this.http.patch(this.apiUrl, formData);
  }
  removeCandidato(id:string){
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
}