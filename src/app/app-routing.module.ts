// app-routing.module.ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CandidatoComponent } from './candidato/candidato.component';
import { CandidaturaComponent } from './candidatura/candidatura.component';
import { VagaComponent } from './vaga/vaga.component';

const routes: Routes = [
  { path: '', redirectTo: '/candidatos', pathMatch: 'full' }, // Rota inicial para candidatos
  { path: 'candidatos', component: CandidatoComponent },
  { path: 'candidatura/:id', component: CandidaturaComponent },
  { path: 'vagas', component: VagaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
