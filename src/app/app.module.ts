import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { CandidatoComponent } from './candidato/candidato.component';
import { AppRoutingModule } from './app-routing.module';
import { PoTableModule,PoGridModule,PoButtonModule,PoFieldModule } from '@po-ui/ng-components';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VagaComponent } from './vaga/vaga.component';
import { CandidaturaComponent } from './candidatura/candidatura.component';




@NgModule({
  declarations: [
    AppComponent,
    CandidatoComponent,
    VagaComponent,
    CandidaturaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    PoButtonModule,
    PoGridModule,
    PoTableModule,
    PoFieldModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
